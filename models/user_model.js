const mongoose = require("mongoose");

const User = mongoose.model(
  "User",
  new mongoose.Schema(
    {
      name: String,
      username: String,
      email: String,
      password: String,
      item: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "item",
      }],
      roles: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Role",
        },
      ],
    },
    {
      timestamps: true,
    }
  ),
  "User"
);

module.exports = User;
