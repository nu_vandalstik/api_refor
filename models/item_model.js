const mongoose = require("mongoose");

const item = mongoose.model(
  "item",
  new mongoose.Schema(
    {
      foto: String,
      judul: String,
      deskripsi: String,
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      },
      dibeli : [ 
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "User",
        }
      ],
    },
    {
      timestamps: true,
    }
  ),
  "item"
);

module.exports = item
