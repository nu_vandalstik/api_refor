const db ={}

db.role = require("./roles_model")
db.user = require('./user_model')
db.cart = require('./cart_model')
db.item = require('./item_model')
db.payment = require('./payment_model')

db.ROLES = ["user", "admin", "vendor"];

module.exports = db;
