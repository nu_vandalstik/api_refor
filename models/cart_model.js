const mongoose = require("mongoose");

const Cart = mongoose.model(
  "Cart",
  new mongoose.Schema(
      {
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
    },
    catatan: String,
    bid: Number,
    konfirmasi: String,
    item: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "item",
    },
  },
  {
      timestamps:true
  }
  ),
  "Cart"
);

module.exports = Cart