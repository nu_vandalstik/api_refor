const mongoose = require("mongoose");

const Payment = mongoose.model(
  "Payment",
  new mongoose.Schema(
      {
          card: String,
          name: String,
          cardNumber: String,
          expire: String,
          cvv:String,
          userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User",
          },
          item:{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Item",
          }
  },
  {
      timestamps:true
  }
  ),
  "Payment"
);

module.exports = Payment