const db = require("../models");
const mongoose = require("mongoose");
const Cart = db.cart;
const Item = db.item;

exports.cart_post = (req, res) => {
  const cart = new Cart({
    userId: req.userId,
    catatan: req.body.catatan,
    bid: req.body.bid,
    konfirmasi: req.body.konfirmasi || "Belum di konfirmasi",
  });
  cart.save((err, cart) => {
    if (err) {
      return res.status(500).send({
        message: err,
      });
    }

    if (req.body.iditem) {
      Item.findOne(
        { _id: mongoose.Types.ObjectId(req.body.iditem) },
        async (err, item) => {
          if (err) {
            return res.status(500).send({
              message: err,
            });
          }

          // console.log(item)
          cart.item = item._id;

          cart.save((err) => {
            if (err) {
              return res.status(500).send({
                message: err,
              });
            }

            res.status(200).send({
              data: cart,
            });
          });
        }
      );
    } else {
      res.status(404).send({
        message: "Data Tidak Tersedia",
      });
    }
  });
};

exports.getAll = (req, res) => {
  console.log("masuk");
  Cart.find()
    .populate("userId")
    .populate("item")
    .exec()
    .then((data) => {
      res.status(200).send({
        data: data,
      });
    })
    .catch((err) => console.log(err));
};

exports.getCartById = (req, res, next) => {
  const id = req.params.codeId;

  Cart.findById(id)
    .populate("userId")
    .populate("item")
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Cart Tidak Di Temukan",
        });
      }
      res.status(200).json({
        message: "Data Code Berhasil DiPanggil!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};


exports.deleteCart = (req,res,next)=>{
  const id = req.params.codeId

  Cart.findById(id).then((cart) =>{
    if(!cart){
      res.status(404).send({
        message: "Data Cart Tidak Ditemukan!"
      })
    }

    return Cart.findByIdAndRemove(id)

  }).then((result) =>{
    res.status(200).json({
      message: "Data Cart Berhasil di Hapus!",
      data : result
    })
  }).catch((err) =>{
    next(err)
  })
}


