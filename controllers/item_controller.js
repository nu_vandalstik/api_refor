const db = require("../models");

const Item = db.item;

exports.item_post = (req, res) => {
  const item = new Item({
    foto: req.body.foto,
    judul: req.body.judul,
    deskripsi: req.body.deskripsi,
    userId: req.userId,
  });

  item.save((err, item) => {
    if (err) {
      return res.status(500).send({
        message: err,
      });
    }

    res.status(200).send({
      data: item,
    });
  });
};

exports.getAll = (req, res) => {
  Item.find()
    .find()
    .populate("userId")
    .populate("dibeli")
    .exec()
    .then((data) => {
      res.status(200).send({
        data: data,
      });
    })
    .catch((err) => err);
};

exports.getItemById = (req, res, next) => {
  const id = req.params.codeId;

  Item.findById(id)
    .populate("userId")
    .populate("dibeli")
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Item Tidak Ditemukan",
        });
      }
      res.status(200).json({
        message: "Data Item Berhasil DiPanggil!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.deleteItembyId = (req, res, next) => {
  const id = req.params.codeId;

  Item.findById(codeId)
    .then((post) => {
      if (!post) {
        res.status(404).send({
          message: "Data Item Tidak Ditemukan",
        });
      }
      return Code.findByIdAndRemove(codeId);
    })
    .then((result) => {
      res.status(200).json({
        message: "Data Item Berhasil DiHapus!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.updateItem = (req, res, next) => {
  const id = req.params.codeId;

  Item.findById(id)
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Data Tidak Ditemukan!",
        });
      }

      (result.foto = req.body.foto),
        (result.judul = req.body.judul),
        (result.deskripsi = req.body.deskripsi);
      return result.save();
    })
    .then((result) => {
      res.status(200).send({
        message: "Update Item Berhasul!",
        data: result,
      });
    });
};
