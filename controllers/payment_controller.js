const db = require("../models");
const mongoose = require("mongoose");
const User = db.user; // tambahhin array item
const Payment = db.payment; // utama
const Item = db.item; // tambahin array data si refor.item

exports.payment = async (req, res) => {
  const pay = new Payment({
    card: req.body.card,
    name: req.body.name,
    cardNumber: req.body.cardNumber,
    expire: req.body.expire,
    cvv: req.body.cvv,
    userId: req.userId,
  });
  pay.save(async (err, newpay) => {
    if (err) {
      return res.status(500).send({
        message: err,
      });
    } else if (req.body.item) {
      await Item.updateOne(
        { _id: mongoose.Types.ObjectId(req.body.item) },
        { $push: { dibeli: mongoose.Types.ObjectId([req.userId]) } }
      );
    }

    await Payment.updateOne(
      { _id: mongoose.Types.ObjectId(newpay._id) },
      { $push: { item: mongoose.Types.ObjectId(req.body.item) } }
    );
    await User.updateOne(
      { _id: mongoose.Types.ObjectId(req.userId) },
      { $push: { item: mongoose.Types.ObjectId([req.body.item]) } }
    );
    res.status(200).send({
      data: newpay,
    });
  });
};

exports.getAll = (req, res) => {
  Payment.find()
    .populate("userId")
    .populate("item")
    .exec()
    .then((data) => {
      res.status(200).send({
        data: data,
      });
    })
    .catch((err) => err);
};
