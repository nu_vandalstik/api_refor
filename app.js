const express = require("express");
const app = express();

const cors = require("cors");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

require("dotenv").config();
var corsOptions = {
  origin: "*",
};

const db = require("./models/index"); // kalau nama index, gausah kasih tau nama file
const Role = db.role;
const userAuth = require("./routes/auth");
const item = require("./routes/item_rotes");
const cart = require("./routes/cart_routes");
const pay = require('./routes/payment.routes')
app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/v1", userAuth);
app.use("/v1", item);
app.use("/v1", cart);
app.use("/v1", pay);

function init() {
  Role.estimatedDocumentCount((err, count) => {
    if (!err & (count == 0)) {
      new Role({
        name: "user",
      }).save((err) => {
        if (err) {
          console.log("error", err);
        }
        console.log("berhasil menambah role user");
      });

      new Role({
        name: "admin",
      }).save((err) => {
        if (err) {
          console.log("error", err);
        }
        console.log("berhasil menambah role admin");
      });

      new Role({
        name: "vendor",
      }).save((err) => {
        if (err) {
          console.log("error", err);
        }
        console.log("berhasil menambah role vendor");
      });
    }
  });
}

app.get("/", (req, res) => {
  res.send("Hello My Best Friend :D");
});

mongoose
  .connect(`${process.env.mongo_uri}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    app.listen(
      process.env.port,
      console.log("koneksi berhasil di port " + process.env.port)
    ),
      init();
  })
  .catch((err) => console.log(err));
