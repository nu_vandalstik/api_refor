const express = require("express");
const router = express.Router();

const { authJwt } = require("../middlewares");
var bodyParser = require("body-parser"); // get body-parser
router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true })); // for parsing
// const verifySignUp = require("../middlewares/verifySignup");
const cart = require("../controllers/cart_controller");

router.post("/cart", [authJwt.verifyToken], cart.cart_post);

router.get("/allcart", [authJwt.verifyToken], cart.getAll);
router.get('/getcart/:codeId', [authJwt.verifyToken], cart.getCartById)
router.delete("/deletecart/:codeId", [authJwt.verifyToken], cart.deleteCart)


module.exports = router;
