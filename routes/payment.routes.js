const express = require("express");
const router = express.Router();

const { authJwt } = require("../middlewares");
var bodyParser = require("body-parser"); // get body-parser
router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true })); // for parsing
// const verifySignUp = require("../middlewares/verifySignup");
const payment = require("../controllers/payment_controller");


router.post(
  "/payment",[authJwt.verifyToken],
  payment.payment
);

router.get('/allpayment', [authJwt.verifyToken], payment.getAll)
module.exports = router;
