const express = require("express");
const router = express.Router();

const { authJwt } = require("../middlewares");
var bodyParser = require("body-parser"); // get body-parser
router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true })); // for parsing
// const verifySignUp = require("../middlewares/verifySignup");
const item = require("../controllers/item_controller");


router.post(
  "/item",[authJwt.verifyToken],
  item.item_post
);

router.get("/allitem", [authJwt.verifyToken], item.getAll)
router.get('/item/:codeId', [authJwt.verifyToken], item.getItemById)
router.delete('/deleteitem/:codeId', [authJwt.verifyToken], item.deleteItembyId)
router.put('/updateitem:codeId', [authJwt.verifyToken], item.updateItem)


module.exports = router;
